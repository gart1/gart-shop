<?php

namespace Database\Seeders;

use App\Models\Subcategory;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

use Illuminate\Support\Str;

class SubcategorySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $subcategories = [
        [
            // Ropa
            'category_id' => 1,
            'name' => 'Playeras',
            'slug' => Str::slug('Playeras'),
            'color' => true
        ],
        [
            'category_id' => 1,
            'name' => 'Gorras',
            'slug' => Str::slug('Gorras'),
        ],
        [
            'category_id' => 1,
            'name' => 'Sudaderas',
            'slug' => Str::slug('Sudaderas'),
            'color' => true
        ],

        //Artesanias
        [
            'category_id' => 2,
            'name' => 'Manualidades',
            'slug' => Str::slug('Manualidades'),
            'color' => true
        ],
        [
            'category_id' => 2,
            'name' => 'Alebrijes',
            'slug' => Str::slug('Alebrijes'),
            'color' => true
        ],

        //Joyeria
        [
            'category_id' => 3,
            'name' => 'Cadenas',
            'slug' => Str::slug('Cadenas'),
            'color' => true
        ],
        [
            'category_id' => 3,
            'name' => 'Anillos',
            'slug' => Str::slug('Anillos'),
            'color' => true
        ],
        [
            'category_id' => 3,
            'name' => 'Esclavas',
            'slug' => Str::slug('Esclavas'),
            'color' => true
        ],
        

        //Mezcal
        [
            'category_id' => 3,
            'name' => 'Mezcal',
            'slug' => Str::slug('Mezcal'),
            'color' => true
        ],
        [
            'category_id' => 3,
            'name' => 'Vasos',
            'slug' => Str::slug('Vasos'),
            'color' => true
        ],

    ];

    foreach ($subcategories as $subcategory) {
        
        Subcategory::factory(1)->create($subcategory);

    }
    }
}
