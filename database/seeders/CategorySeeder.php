<?php

namespace Database\Seeders;

use App\Models\Brand;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;


use Illuminate\Support\Str;
use Illuminate\Database\Seeder;

use App\Models\Category;

class CategorySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $categories = [
            [
                'name' => 'Ropa',
                'slug' => Str::slug('Ropa'),
                'icon' => '<i class="fa-solid fa-shirt"></i>'
            ],
            [
                'name' => 'Artesanias',
                'slug' => Str::slug('Artesanias'),
                'icon' => '<i class="fa-solid fa-palette"></i>'
            ],            [
                'name' => 'Joyeria',
                'slug' => Str::slug('Joyeria'),
                'icon' => '<i class="fa-solid fa-gem"></i>'
            ],            [
                'name' => 'Mezcal',
                'slug' => Str::slug('Mezcal'),
                'icon' => '<i class="fa-solid fa-wine-bottle"></i>'
            ],
        ];

        foreach ($categories as $category) {
            $category = Category::factory(1)->create($category)->first();

            $brands = Brand::factory(4)->create();

            foreach ($brands as $brand) {
                $brand->categories()->attach($category->id);
            }
        }
    }
}