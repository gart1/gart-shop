<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Size extends Model
{
    use HasFactory;

    protected $fillable = ['name', 'product_id'];


    //1 a M inverse 
    public function product(){
        return $this->belongsTo(Product::class);
    }

    //M a m
    public function colors(){
        return $this->belongsToMany(Color::class);
    }
}
