<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Brand extends Model
{
    use HasFactory;

    protected $fillable = ['name'];

    // 1 a M
    public function products(){
        return $this->hasMany(Product::class);
    }

    //M a M 
    public function categories(){
        return $this->belongsToMany(Category::class);
    }

}
