<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    use HasFactory;

    const BORRADOR = 1;
    const PUBLICADO =2; 

    protected $guarded = ['id', 'created_at', 'updated_at'];

    //1 a M
    public function size(){
        return $this->hasMany(Size::class);
    }

    //1 a M Inverse
    public function brand(){
        return $this->belongsTo(Brand::class);
    }

    public function subcategory(){
        return $this->belongsTo(Subcategory::class);
    }

    //M a m 
    public function colors(){
        return $this->belongsToMany(Color::class);
    }

    //1 a M Polimorfic
    public function images(){
        return $this->morphMany(Image::class, 'imageable');
    }
}
